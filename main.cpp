#include <iostream>
#include "Inherit.hpp"

int main() {

    Inherit t;
    Child c;
    Child c2;
    Child c3;

//    t.SaySmtLoving();

    std::cout << c.getIter() << std::endl;

    c.set_x(6);
    c2.set_x(36);
//    std::cout << c.get_x() << std::endl;
//    std::cout << c2.get_x() << std::endl;
//    std::cout << c.get_x() << std::endl;
//
//    std::cout << c.getIter() << std::endl;

    c3.addRes(c, c2);

    std::cout << c3.getRes() << std::endl;

    return 0;
}