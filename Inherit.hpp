//
// Created by Roman Ishchenko on 11/18/17.
//

#ifndef INHERITANCE_INHERIT_HPP
#define INHERITANCE_INHERIT_HPP

#include <iostream>

class Inherit {

private:
    int _x;
    int _res;
    static int _iter;
public:
    Inherit(int x, int res);
    Inherit();
    virtual ~Inherit();

    int get_x() const;
    void set_x(int _x);
    int     getIter();
    int     getRes();

    void    addRes(Inherit c1, Inherit c2);
    void    SaySmtLoving();
};


class Child : public Inherit {


};

#endif //INHERITANCE_INHERIT_HPP
